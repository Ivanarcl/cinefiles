import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MovieService } from '../movie.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.page.html',
  styleUrls: ['./movies.page.scss'],
})
export class MoviesPage implements OnInit {


  resultados: Observable<any>;
  nombre ='';

  public buttonClicked: boolean = false;

  constructor(private movieService: MovieService, private storage: Storage) { }
 
  ngOnInit() {
  }

  public onButtonClick() {

    this.buttonClicked = !this.buttonClicked;
}

  busqueda(){
    this.resultados = this.movieService.getPeli(this.nombre);
  }

}
