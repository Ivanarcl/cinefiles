import { MovieService } from '../movie.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

 
@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.page.html',
  styleUrls: ['./movie-details.page.scss'],
})
export class MovieDetailsPage implements OnInit {
 
  information = null;
  
  constructor( public router: Router, public service : MovieService, private activatedRoute: ActivatedRoute, private movieService: MovieService) { }
 
  ngOnInit() {
    
  }
  ionViewWillEnter(){
    // Get the ID that was passed with the URL
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    // Get the information from the API
    this.movieService.getDetalles(id).subscribe(result => {
      this.information = result;
    });
  }
  
  click(){
   let data = [];
    data.push(this.information);
    this.service.setSelectedMovies(data);
    this.router.navigate(['/my-movies']);
  }

}