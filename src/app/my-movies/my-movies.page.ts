import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MovieService } from '../movie.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-movies',
  templateUrl: './my-movies.page.html',
  styleUrls: ['./my-movies.page.scss'],
})
export class MyMoviesPage implements OnInit {
  movies: any;
  seleccionadas = [];
  constructor(private service: MovieService, private storage: Storage, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.service.loadSelectedMovies();
    this.seleccionadas = this.service.getSelectedMovies();
    console.log("selected", this.seleccionadas);
  }
  ionViewWillEnter(){
    
  }

}
