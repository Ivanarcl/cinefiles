import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
const routes: Routes = [
  { path: '', redirectTo: 'movies', pathMatch: 'full' },
  { path: 'movies', loadChildren: './movies/movies.module#MoviesPageModule' },
  { path: 'movies/:id', loadChildren: './movie-details/movie-details.module#MovieDetailsPageModule' },
  {
    path: 'my-movies',
    loadChildren: () => import('./my-movies/my-movies.module').then( m => m.MyMoviesPageModule)
  },
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 