import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
 
@Injectable({
  providedIn: 'root'
})
export class MovieService {
  movies = [];
  constructor(private http: HttpClient, private storage: Storage) { }
 
  /*Acceder a películas en la búsqueda por título*/ 
  getPeli(titulo: string): Observable<any> { 
    return this.http.get(`http://www.omdbapi.com/?s=${encodeURI(titulo)}&apikey=2a993467`).pipe(
      map(results => results['Search']) /*Mapea para que salga como resultado lo que se buscó*/
    );
  }
 
  /*Acceder a los datos de cada película*/
  getDetalles(id) {
    return this.http.get(`http://www.omdbapi.com/?i=${id}&plot=full&apikey=2a993467`);
  }
  setSelectedMovies(watch){
    this.movies = watch;
    this.storage.set('peli', this.movies);
  }
  getSelectedMovies(){
    return this.movies;
  }
  loadSelectedMovies(){
    this.storage.get('peli').then((val) => {
      console.log(val);
      if(val!= null){
        this.movies = val;
      }
    })
  }

}


